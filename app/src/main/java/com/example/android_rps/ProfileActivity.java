package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    private OwnUser user = null;
    private static boolean USER_FETCH = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        OwnDB db = new OwnDB();
        db.getAllUsers().addAllUsersListener((OldUsers, NewUsers)->
            {
                //TODO
                //Eventuellement mettre en amont une ProgressBar
                user = OwnUser.ExtractUser(NewUsers, db.getCurrentUser().getUid());
                Afficher(user, user.ExtractRanking(NewUsers) + 1);
                if (USER_FETCH == false)
                {
                    BindEditText();
                    USER_FETCH = true;
                }
            }
        );
        db.ExtractUsers(this, true);
    }

    public void ToReturn(View view) {
        OwnRedirect.ToReturn(this);
    }

    public void Afficher(OwnUser user, int Classement)
    {
        EditText Name = findViewById(R.id.lbl_nom_profile);
        EditText FirstName = findViewById(R.id.lbl_prenom_profile);
        EditText UserName = findViewById(R.id.lbl_pseudo_profile);
        TextView Ranking = findViewById(R.id.lbl_affichageclassement_profile);
        TextView Score = findViewById(R.id.lbl_affichagescore_profile);
        Name.setText(user.name);
        FirstName.setText(user.firstName);
        UserName.setText(user.username);
        Ranking.setText(String.valueOf(Classement));
        Score.setText(String.valueOf(user.score));
    }

    public void Disconnect(View view) {
        stopService(new Intent(this, NotificationService.class));
        new OwnDB().Disconnect();
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void BindEditText()
    {
        EditText UserName = findViewById(R.id.lbl_pseudo_profile);
        UserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (user != null && !(user.username.contentEquals(s)))
                {
                    user.username = s.toString();
                    updateProfile(user);
                }
            }
        });
        EditText Name = findViewById(R.id.lbl_nom_profile);
        Name.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (user != null && !(user.name.contentEquals(s)))
                {
                    user.name = s.toString();
                    updateProfile(user);
                }
            }
        });
        EditText FirstName = findViewById(R.id.lbl_prenom_profile);
        FirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (user != null && !(user.firstName.contentEquals(s)))
                {
                    user.firstName = s.toString();
                    updateProfile(user);
                }
            }
        });
    }

    private void updateProfile(OwnUser user)
    {
        new OwnDB().UpdateUser(this, user);
    }
}
