package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class VictoireActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.victoire_activity);
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView affichagePoints = findViewById(R.id.lbl_points_victoire);
        TextView affichageResultats = findViewById(R.id.lbl_resultat_victoire);
        int nbPoints = getIntent().getExtras().getInt(OwnRedirect.NBPOINT);
        int resultatPartie = getIntent().getExtras().getInt(OwnRedirect.RESULTAT);
        String strResultatPartie = "";
        if (resultatPartie == OwnGame.WIN) {
            strResultatPartie = "VICTOIRE";
            if (nbPoints != 0) {
                affichagePoints.setText("Vous avez gagné "+nbPoints+" points.");
            }
        } else if (resultatPartie == OwnGame.EQUALITY) {
            strResultatPartie = "ÉGALITÉ";
        } else if (resultatPartie == OwnGame.LOOSE) {
            strResultatPartie = "DÉFAITE";
        }
        affichageResultats.setText(strResultatPartie);
    }


    public void ToChoixDiff(View view) {
        Intent destination = new Intent(this, choixDifficulteActivity.class);
        destination.putExtra(OwnRedirect.DONTREDIRECT, true);
        OwnRedirect.GoTo(this, destination);
    }

    public void ToClassement(View view) {
        Intent destination = new Intent(this, ClassementActivity.class);
        OwnRedirect.GoTo(this, destination, OwnRedirect.CHOIX);
    }
}
