package com.example.android_rps;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class OwnGame implements Parcelable
{
    public static final int MANCHES = 5;
    public static final int LOOSE = -1;
    public static final int WIN = 1;
    public static final int EQUALITY = 0;
    public static final int CONTINUE = 2;
    public ArrayList<Integer> manches;
    public String difficulty;
    public int position;

    public OwnGame(String difficulty)
    {
        manches = new ArrayList<Integer>(MANCHES);
        position = 0;
        this.difficulty = difficulty;
    }

    public OwnGame()
    {
        manches = new ArrayList<Integer>(MANCHES);
        position = 0;
        difficulty = "";
    }

    protected OwnGame(Parcel in)
    {
        difficulty = in.readString();
        position = in.readInt();
        manches = new ArrayList<Integer>(MANCHES);
        int max = in.readInt();
        for (int i = 0; i < max; i++)
        {
            manches.add(in.readInt());
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(difficulty);
        dest.writeInt(position);
        dest.writeInt(manches.size());
        for (int i = 0; i < manches.size(); i++)
        {
            dest.writeInt(manches.get(i));
        }
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<OwnGame> CREATOR = new Creator<OwnGame>()
    {
        @Override
        public OwnGame createFromParcel(Parcel in)
        {
            return new OwnGame(in);
        }

        @Override
        public OwnGame[] newArray(int size)
        {
            return new OwnGame[size];
        }
    };

    public int EndTurn(Integer result)
    {
        int game = WhoIsTheWinner();
        if (position < MANCHES && game == CONTINUE)
        {
            manches.add(result);
            position++;
        }
        return game;
    }

    public int WhoIsTheWinner()
    {
        if (NbWin() > MANCHES / 2)
            return WIN;
        else
            if (position == MANCHES)
                return LOOSE;
            else
                return CONTINUE;
    }

    public int NbWin()
    {
        int nb = 0;
        for (Integer manche : manches)
            if (manche == 1)
                nb++;
        return nb;
    }

    public int NbLoose()
    {
        int nb = 0;
        for (Integer manche : manches)
            if (manche == -1)
                nb++;
        return nb;
    }

    @Override
    public String toString()
    {
        String str = "OwnGame:{\"position\":\"" + position + "\",\"difficulty:\":\"" + difficulty + "\",\"manches\":[";
        for (int i = 0; i < manches.size(); i++)
        {
            if (i > 0)
                str = str + ",";
            str = str + manches.get(i);
        }
        str = str + "]}";
        return str;
    }

    public int EndTurnReally(Integer result)
    {
        int game = WhoIsTheWinnerReally();
        if (position < MANCHES && game == CONTINUE)
        {
            manches.add(result);
            position++;
        }
        return game;
    }

    public int WhoIsTheWinnerReally()
    {
        if (NbWin() > MANCHES / 2)
            return WIN;
        else
        if (NbLoose() > MANCHES / 2)
            return LOOSE;
        else
        if (position == MANCHES)
            if (NbWin() > NbLoose())
                return WIN;
            else
            if (NbWin() < NbLoose())
                return LOOSE;
            else
                return EQUALITY;
        else
            return CONTINUE;
    }
}
