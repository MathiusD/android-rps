package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

public class choixDifficulteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choix_difficulte_activity);
        boolean redirect = true;

        if (getIntent().hasExtra(OwnRedirect.DONTREDIRECT))
        {
            if (getIntent().getExtras().getBoolean(OwnRedirect.DONTREDIRECT))
            {
                redirect = false;
            }
        }
        if (redirect == true)
        {
            OwnDB db = new OwnDB();
            db.getCurrentUserDB().addAllUsersListener((OldUser, NewUser) ->
                {
                    if (NewUser.lastGame.position > 0)
                    {
                        Bundle data = new Bundle();
                        data.putParcelable(OwnRedirect.USER, NewUser);
                        Intent destination = new Intent(this, JeuActivity.class);
                        destination.putExtra(OwnRedirect.DIFFICULTE, NewUser.lastGame.difficulty);
                        destination.putExtra(OwnRedirect.DATA, data);
                        finish();
                        startActivity(destination);
                    }
                }
            );
            db.ExtractCurrentUser(this);
        }
    }

    public void ToRegles(View view) {
        Intent destination = new Intent(this, ReglesActivity.class);
        GoTo(destination);
    }

    public void ToClassement(View view) {
        Intent destination = new Intent(this, ClassementActivity.class);
        GoTo(destination);
    }

    public void ToProfile(View view) {
        Intent destination = new Intent(this, ProfileActivity.class);
        GoTo(destination);
    }

    public void GoTo(Intent destination)
    {
        OwnRedirect.GoTo(this, destination, OwnRedirect.CHOIX, OwnRedirect.CHOIX);
    }

    public void ChoixDiff(View view) {
        RadioGroup groupe = findViewById(R.id.rdg_choixdif);
        int choix = groupe.getCheckedRadioButtonId();
        String diff = "";
        if(choix == R.id.rdo_spockdif_choixdif) {
            diff = Difficulte.SPOCK.getNom();
        } else if (choix == R.id.rdo_facile_choixdif) {
            diff = Difficulte.FACILE.getNom();
        } else if (choix == R.id.rdo_normal_choixdif) {
            diff = Difficulte.NORMAL.getNom();
        } else if (choix == R.id.rdo_difficile_choixdif){
            diff = Difficulte.DIFFICILE.getNom();
        } else if (choix == R.id.rdo_impossible_choixdif){
            diff = Difficulte.IMPOSSIBLE.getNom();
        }
        if (choix != -1){
            Intent destination = new Intent(this, JeuActivity.class);
            destination.putExtra(OwnRedirect.DIFFICULTE, diff);
            finish();
            startActivity(destination);
        } else {
            Toast.makeText(this, "Veuillez choisir une difficulté.", Toast.LENGTH_LONG).show();
        }
    }
}
