package com.example.android_rps;

import java.util.ArrayList;

class AllUsers
{
    private ArrayList<OwnUser> data;
    private final ArrayList<AllUsersListener> allUsersListeners;

    AllUsers ()
    {
        allUsersListeners = new ArrayList<>();
    }

    void setData(ArrayList<OwnUser> data)
    {
        ArrayList<OwnUser> old = this.data;
        this.data = data;
        for (AllUsersListener ping : allUsersListeners)
        {
            ping.AllUsersValueChanged(old, data);
        }
    }

    void addAllUsersListener(AllUsersListener allUsersListener)
    {
        allUsersListeners.add(allUsersListener);
    }

    ArrayList<OwnUser> getData()
    {
        return data;
    }
}
