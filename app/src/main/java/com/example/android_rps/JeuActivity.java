package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.Objects;
import java.util.Random;

public class JeuActivity extends AppCompatActivity {

    private OwnUser user;
    private OwnDB ownDB;
    private int result;
    private int nbpoint = 0;
    private int resultmanche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jeu_activity);
        if (getIntent().getExtras().containsKey(OwnRedirect.DATA))
        {
            ownDB = new OwnDB();
            this.user = Objects.requireNonNull(getIntent().getExtras().getBundle(OwnRedirect.DATA)).getParcelable(OwnRedirect.USER);
            if (user.lastGame.position == 0)
                user.lastGame = new OwnGame(getIntent().getExtras().getString(OwnRedirect.DIFFICULTE));
            UpdatePuces();
        }
        else
        {
            SetupOwnGame(getIntent().getExtras().getString(OwnRedirect.DIFFICULTE));
        }
    }

    public void SetupOwnGame(String difficulty)
    {
        this.ownDB = new OwnDB();
        this.user = null;
        GetCurrentUser(difficulty);
    }

    public void GetCurrentUser(final String difficulty)
    {
        this.ownDB.getCurrentUserDB().addAllUsersListener((OldUser, NewUser) ->
            {
                user = NewUser;
                user.lastGame = new OwnGame(difficulty);
            }
        );
        this.ownDB.ExtractCurrentUser(this);
    }

    public void UpdatePuces()
    {
        ImageView puce1 = findViewById(R.id.img_puce1_jeu);
        ImageView puce2 = findViewById(R.id.img_puce2_jeu);
        ImageView puce3 = findViewById(R.id.img_puce3_jeu);
        ImageView puce4 = findViewById(R.id.img_puce4_jeu);
        if (user.lastGame.manches.size() > 0)
        {
            puce1.setImageDrawable(DeterminePuce(user.lastGame.manches.get(0)));
            if (user.lastGame.manches.size() > 1)
            {
                puce2.setImageDrawable(DeterminePuce(user.lastGame.manches.get(1)));
                if (user.lastGame.manches.size() > 2)
                {
                    puce3.setImageDrawable(DeterminePuce(user.lastGame.manches.get(2)));
                    if (user.lastGame.manches.size() > 3)
                    {
                        puce4.setImageDrawable(DeterminePuce(user.lastGame.manches.get(3)));
                    }
                }
            }
        }
    }

    public Drawable DeterminePuce(int result)
    {
        Drawable imagePuce = null;
        if(result == OwnGame.WIN) {
            imagePuce = getDrawable(R.drawable.puce_verte);
        } else if(result == OwnGame.LOOSE) {
            imagePuce = getDrawable(R.drawable.puce_rouge);
        } else if(result == OwnGame.EQUALITY) {
            imagePuce = getDrawable(R.drawable.puce_bleue);
        }
        return imagePuce;
    }

    public void ToRegles(View view) {
        Intent destination = new Intent(this, ReglesActivity.class);
        GoTo(destination);
    }

    public void ToClassement(View view) {
        Intent destination = new Intent(this, ClassementActivity.class);
        GoTo(destination);
    }

    public void ToProfile(View view) {
        Intent destination = new Intent(this, ProfileActivity.class);
        GoTo(destination);
    }

    public void GoTo(Intent destination)
    {
        Bundle data = new Bundle();
        Bundle data_ = new Bundle();
        data_.putParcelable(OwnRedirect.USER, user);
        data.putBundle(OwnRedirect.DATA, data_);
        data.putString(OwnRedirect.DIFFICULTE, user.lastGame.difficulty);
        OwnRedirect.GoTo(this, destination, OwnRedirect.JEU, OwnRedirect.JEU, data);
    }

    public void RadioGroupBoycott(View view) {
        RadioButton pierre = findViewById(R.id.rdo_pierre_jeu);
        RadioButton papier = findViewById(R.id.rdo_papier_jeu);
        RadioButton ciseaux = findViewById(R.id.rdo_ciseaux_jeu);
        RadioButton spock = findViewById(R.id.rdo_spock_jeu);
        RadioButton lesard = findViewById(R.id.rdo_lesard_jeu);
        String selection = "";
        pierre.setChecked(false);
        papier.setChecked(false);
        ciseaux.setChecked(false);
        spock.setChecked(false);
        lesard.setChecked(false);
        if (view.getId() == pierre.getId()) {
            pierre.setChecked(true);
            selection = "pierre";
        } else if (view.getId() == papier.getId()){
            papier.setChecked(true);
            selection = "papier";
        } else if (view.getId() == ciseaux.getId()){
            ciseaux.setChecked(true);
            selection = "ciseaux";
        } else if (view.getId() == spock.getId()){
            spock.setChecked(true);
            selection = "spock";
        } else if (view.getId() == lesard.getId()){
            lesard.setChecked(true);
            selection = "lesard";
        }
        SelectionRadio(selection);
    }

    public void SelectionRadio(String selection) {
        ImageView pierre = findViewById(R.id.img_blackrock_jeu);
        ImageView papier = findViewById(R.id.img_blackpaper_jeu);
        ImageView ciseaux = findViewById(R.id.img_blackscissors_jeu);
        ImageView spock = findViewById(R.id.img_blackspock_jeu);
        ImageView lesard = findViewById(R.id.img_blacklizard_jeu);
        pierre.setImageAlpha(255);
        papier.setImageAlpha(255);
        ciseaux.setImageAlpha(255);
        spock.setImageAlpha(255);
        lesard.setImageAlpha(255);
        if (selection.equals("pierre")) {
            pierre.setImageAlpha(0);
        } else if (selection.equals("papier")) {
            papier.setImageAlpha(0);
        } else if (selection.equals("ciseaux")) {
            ciseaux.setImageAlpha(0);
        } else if (selection.equals("spock")) {
            spock.setImageAlpha(0);
        } else if (selection.equals("lesard")) {
            lesard.setImageAlpha(0);
        }
    }

    public void LancementTour(View view) {
        GameChoice element = SelectionElement(view);
        if (element != null)
        {
            if (user != null)
            {
                Difficulte difficulte = SelectionDifficulte(view);
                GameChoice elementIA = SelectionElementIA(view, element, difficulte);
                GestionPartie();
                AffichagePartie(view, elementIA);
            }
            else
            {
                Toast.makeText(this, "Soucis de connection avec les serveurs. Veuillez réessayer.", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(this, "Veuillez sélectionner votre choix, merci.", Toast.LENGTH_LONG).show();
        }
    }

    public GameChoice SelectionElement(View view) {
        RadioButton pierre = findViewById(R.id.rdo_pierre_jeu);
        RadioButton papier = findViewById(R.id.rdo_papier_jeu);
        RadioButton ciseaux = findViewById(R.id.rdo_ciseaux_jeu);
        RadioButton spock = findViewById(R.id.rdo_spock_jeu);
        RadioButton lesard = findViewById(R.id.rdo_lesard_jeu);
        String elementChoisi = "";
        if (pierre.isChecked() == true){
            elementChoisi = GameChoice.ROCK.getName();
        } else if (papier.isChecked() == true){
            elementChoisi = GameChoice.PAPER.getName();
        } else if (ciseaux.isChecked() == true){
            elementChoisi = GameChoice.SCISSORS.getName();
        } else if (spock.isChecked() == true){
            elementChoisi = GameChoice.SPOCK.getName();
        } else if (lesard.isChecked() == true){
            elementChoisi = GameChoice.LIZARD.getName();
        }
        GameChoice element = null;
        if (elementChoisi.equals(GameChoice.ROCK.getName())){
            element = GameChoice.ROCK;
        } else if (elementChoisi.equals(GameChoice.PAPER.getName())) {
            element = GameChoice.PAPER;
        } else if (elementChoisi.equals(GameChoice.SCISSORS.getName())) {
            element = GameChoice.SCISSORS;
        } else if (elementChoisi.equals(GameChoice.SPOCK.getName())) {
            element = GameChoice.SPOCK;
        } else if (elementChoisi.equals(GameChoice.LIZARD.getName())) {
            element = GameChoice.LIZARD;
        }
        return element;
    }

    public Difficulte SelectionDifficulte(View view) {
        String nivDiff = getIntent().getExtras().getString(OwnRedirect.DIFFICULTE);
        Difficulte difficulteActuelle = null;
        if (nivDiff.equals(Difficulte.SPOCK.getNom())) {
            difficulteActuelle = Difficulte.SPOCK;
        } else if (nivDiff.equals(Difficulte.FACILE.getNom())){
            difficulteActuelle = Difficulte.FACILE;
        } else if (nivDiff.equals(Difficulte.NORMAL.getNom())){
            difficulteActuelle = Difficulte.NORMAL;
        } else if (nivDiff.equals(Difficulte.DIFFICILE.getNom())){
            difficulteActuelle = Difficulte.DIFFICILE;
        } else if (nivDiff.equals(Difficulte.IMPOSSIBLE.getNom())){
            difficulteActuelle = Difficulte.IMPOSSIBLE;
        }
        return difficulteActuelle;
    }

    public GameChoice SelectionElementIA(View view, GameChoice element, Difficulte difficulte) {
        int random = new Random().nextInt(10)+1;
        GameChoice elementIA = null;
        if (difficulte == Difficulte.SPOCK) {
            elementIA = Spock(view, element);
        } else {
            if (random <= difficulte.getChanceDefaiteIA()) { //Défaite IA
                elementIA = element.getAdvantage().get(new Random().nextInt(element.getAdvantage().size()));
                this.result = OwnGame.WIN;
            } else if (random <= difficulte.getChanceDefaiteIA()+difficulte.getChanceEgalite()){ //Égalité
                elementIA = element.Neutral().get(new Random().nextInt(element.Neutral().size()));
                this.result = OwnGame.EQUALITY;
            } else { //Victoire IA
                elementIA = element.Frailty().get(new Random().nextInt(element.Frailty().size()));
                this.result = OwnGame.LOOSE;
            }
        }
        return elementIA;
    }

    public GameChoice Spock(View view, GameChoice element) {
        GameChoice elementIA = GameChoice.SPOCK;
        if (element.getAdvantage().contains(elementIA)) {
            this.result = OwnGame.WIN;
        } else if (element == GameChoice.SPOCK) {
            this.result = OwnGame.EQUALITY;
        } else {
            this.result = OwnGame.LOOSE;
        }
        return elementIA;
    }

    public void AffichagePartie(View view, GameChoice elementIA) {
        UpdatePuces();
        int idImageCentrale = 0;
        if (elementIA == GameChoice.ROCK){
            idImageCentrale = R.drawable.pierre;
        } else if (elementIA == GameChoice.PAPER) {
            idImageCentrale = R.drawable.papier;
        } else if (elementIA == GameChoice.SCISSORS) {
            idImageCentrale = R.drawable.ciseaux;
        } else if (elementIA == GameChoice.SPOCK) {
            idImageCentrale = R.drawable.spock;
        } else if (elementIA == GameChoice.LIZARD) {
            idImageCentrale = R.drawable.lesard;
        }
        Intent destination = new Intent(this, AffElemiaActivity.class);
        String vousAvez = "";
        if (resultmanche == OwnGame.WIN){
            vousAvez = "Vous avez gagné cette manche.";
        } else if (resultmanche == OwnGame.LOOSE) {
            vousAvez = "Vous avez perdu cette manche.";
        } else if (resultmanche == OwnGame.EQUALITY) {
            vousAvez = "Égalité.";
        }
        destination.putExtra(OwnRedirect.VOUS_AVEZ, vousAvez);
        destination.putExtra(OwnRedirect.IMG_ELEMENT_IA, idImageCentrale);
        destination.putExtra(OwnRedirect.RESULTAT, result);
        destination.putExtra(OwnRedirect.NBPOINT, nbpoint);
        GoTo(destination);
    }

    public void GestionPartie()
    {
        this.user.lastGame.EndTurn(result);
        resultmanche = user.lastGame.manches.get(user.lastGame.manches.size() - 1);
        this.result = user.lastGame.WhoIsTheWinner();
        if (result != OwnGame.CONTINUE)
        {
            GestionVictoire();
        }
        else
        {
            PushBDD();
        }
    }

    public void GestionVictoire()
    {
        if (result == OwnGame.WIN)
        {
            String nivDiff = getIntent().getExtras().getString(OwnRedirect.DIFFICULTE);
            if (nivDiff.equals(Difficulte.FACILE.getNom())) {
                nbpoint = Difficulte.FACILE.getPointsGagne();
            } else if (nivDiff.equals(Difficulte.NORMAL.getNom())) {
                nbpoint = Difficulte.NORMAL.getPointsGagne();
            } else if (nivDiff.equals(Difficulte.DIFFICILE.getNom())) {
                nbpoint = Difficulte.DIFFICILE.getPointsGagne();
            } else if (nivDiff.equals(Difficulte.IMPOSSIBLE.getNom())) {
                nbpoint = Difficulte.IMPOSSIBLE.getPointsGagne();
            }
        }
        user.score += nbpoint;
        user.lastGame = new OwnGame();
        PushBDD();
    }

    public void PushBDD(){
        PushBDD(false);
    }

    public void PushBDD(boolean waiting){
        if (waiting != false)
        {
            if (this.user != null)
                this.ownDB.UpdateUser(this, this.user);
        }
        else
        {
            while (this.user == null) {
                SystemClock.sleep(1000);
            }
            this.ownDB.UpdateUser(this, this.user);
        }
    }

}
