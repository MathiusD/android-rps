package com.example.android_rps;

import java.util.ArrayList;

public class LastMove {

    private String data;
    private final ArrayList<LastMoveListener> lastMoveListeners;

    LastMove()
    {
        data = null;
        lastMoveListeners = new ArrayList<>();
    }

    void setData(String data)
    {
        String old = this.data;
        this.data = data;
        for (LastMoveListener ping : lastMoveListeners)
        {
            ping.LastMoveValueChanged(old, data);
        }
    }

    void addLastMoveListener(LastMoveListener lastMoveListener)
    {
        lastMoveListeners.add(lastMoveListener);
    }

    String getData()
    {
        return data;
    }
}
