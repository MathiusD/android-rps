package com.example.android_rps;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class NotificationService extends Service {

    private static final String CHANNEL_ID = "Blap";
    private static final String TAG = "OwnNotification";
    private static int NOTIF_ID = 1;

    public NotificationService() {
        OwnDB db = new OwnDB();
        db.getAllUsers().addAllUsersListener((OldUsers, NewUsers)->
        {
            if (OldUsers != null)
            {
                OwnUser user = OwnUser.ExtractUser(NewUsers, db.getCurrentUser().getUid());
                OwnUser olduser = OwnUser.ExtractUser(OldUsers, db.getCurrentUser().getUid());
                int actual_ranking = user.ExtractRanking(NewUsers) + 1;
                int old_ranking = olduser.ExtractRanking(OldUsers) + 1;
                if (actual_ranking != old_ranking) {
                    Log.d(TAG, "RankingChanged");
                    createNotificationChannel();
                    Intent intent = new Intent(this, ClassementActivity.class);
                    intent.putExtra(OwnRedirect.TO, OwnRedirect.CHOIX);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                            .setSmallIcon(R.mipmap.ic_launcher_round)
                            .setContentTitle("Classement")
                            .setContentText("Votre classement a évolué")
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText("Votre classement a évolué, vous êtes passé de " + old_ranking + " à " + actual_ranking))
                            .setContentIntent(pendingIntent)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                    notificationManager.notify(NOTIF_ID, builder.build());
                    NOTIF_ID += 1;
                }
            }
        });
        db.ExtractUsers(true);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "RPS-Notification";
            String description = "For Notification RPS app's";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        onTaskRemoved(intent);
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(),this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
