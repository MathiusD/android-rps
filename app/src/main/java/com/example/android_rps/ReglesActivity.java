package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

public class ReglesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regles_activity);
    }

    public void Retour(View view) {
        OwnRedirect.ToReturn(this);
    }
}
