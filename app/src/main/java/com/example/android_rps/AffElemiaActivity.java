package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class AffElemiaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aff_elemia_activity);
    }

    @Override
    public void onStart() {
        super.onStart();
        int imgIA = getIntent().getExtras().getInt(OwnRedirect.IMG_ELEMENT_IA);
        String vousAvez = getIntent().getExtras().getString(OwnRedirect.VOUS_AVEZ);
        ImageView imageCentrale = findViewById(R.id.img_elementia_elemia);
        TextView lblVousAvez = findViewById(R.id.lbl_vousavez_elemia);
        imageCentrale.setImageDrawable(getDrawable(imgIA));
        lblVousAvez.setText(vousAvez);
    }

    public void ToJeu(View view) {
        OwnUser user = getIntent().getExtras().getBundle(OwnRedirect.DATA_TRANSLATE).getBundle(OwnRedirect.DATA).getParcelable(OwnRedirect.USER);
        if (user.lastGame.position != 0)
        {
            OwnRedirect.ToReturn(this);
        }
        else
        {
            Intent destination = new Intent(this, VictoireActivity.class);
            destination.putExtra(OwnRedirect.RESULTAT, getIntent().getExtras().getInt(OwnRedirect.RESULTAT));
            destination.putExtra(OwnRedirect.NBPOINT, getIntent().getExtras().getInt(OwnRedirect.NBPOINT));
            OwnRedirect.GoTo(this, destination);
        }
    }
}
