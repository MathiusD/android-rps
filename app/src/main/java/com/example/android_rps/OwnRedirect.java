package com.example.android_rps;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class OwnRedirect {

    //Clés de Gestions
    static final String DONTREDIRECT = "DontRedirect";
    static final String DATA_TRANSLATE = "data_translate";
    static final String FROM = "From";
    static final String TO = "To";
    static final String JCJ = "JCJ";
    //Clés de Données
    static final String DATA = "data";
    static final String USER = "user";
    static final String DIFFICULTE = "difficulte";
    static final String VOUS_AVEZ = "vousAvez";
    static final String IMG_ELEMENT_IA = "imgElementIA";
    static final String RESULTAT = "Resultat";
    static final String NBPOINT = "NbPoint";
    //Valeurs de redirection
    static final String CHOIX = "Choix";
    static final String JEU = "Jeu";

    static void ToReturn(AppCompatActivity ctx) {
        if (ctx.getIntent().getExtras().getString(TO).equals(JEU))
            GoTo(ctx, new Intent(ctx, JeuActivity.class));
        else if (ctx.getIntent().getExtras().getString(TO).equals(CHOIX))
            GoTo(ctx, new Intent(ctx, choixDifficulteActivity.class));
    }

    static void GoTo(AppCompatActivity ctx, Intent destination)
    {
        GoTo(ctx, destination, (String) null);
    }

    static void GoTo(AppCompatActivity ctx, Intent destination, Bundle bundle)
    {
        GoTo(ctx, destination, null, bundle);
    }

    static void GoTo(AppCompatActivity ctx, Intent destination, String to)
    {
        GoTo(ctx, destination, to, (String) null);
    }

    static void GoTo(AppCompatActivity ctx, Intent destination, String to, String from)
    {
        GoTo(ctx, destination, to, from, null);
    }

    static void GoTo(AppCompatActivity ctx, Intent destination, String to, Bundle bundle)
    {
        GoTo(ctx, destination, to, null, bundle);
    }

    static void GoTo(AppCompatActivity ctx, Intent destination, String to, String from, Bundle bundle)
    {
        if (to != null)
            destination.putExtra(TO, to);
        if (from != null)
            destination.putExtra(FROM, from);
        if (bundle != null)
            destination.putExtra(DATA_TRANSLATE, bundle);
        if (ctx.getIntent().hasExtra(DATA_TRANSLATE))
            destination.putExtras(ctx.getIntent().getExtras().getBundle(DATA_TRANSLATE));
        ctx.finish();
        ctx.startActivity(destination);
    }
}
