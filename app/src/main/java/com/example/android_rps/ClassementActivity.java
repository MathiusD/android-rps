package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class ClassementActivity extends AppCompatActivity {

    private ArrayList<OwnUser> Users = null;
    private int Classement = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.classement_activity);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        OwnDB db = new OwnDB();
        db.getAllUsers().addAllUsersListener((OldUsers, NewUsers)->
            {
                //TODO
                //Eventuellement mettre en amont une ProgressBar
                if (Users == null)
                {
                    Afficher(NewUsers);
                }
                else
                {
                    int Class = Classement;
                    if (Classement > OldUsers.size() - 1)
                        Class = OldUsers.size() - 1;
                    Classement = OwnUser.ExtractUser(NewUsers, OldUsers.get(Class).uid).ExtractRanking(NewUsers);
                    Afficher(NewUsers, Classement);
                }
                Users = NewUsers;
            }
        );
        db.ExtractUsers(this, true);
    }

    public void ToJeu(View view) {
        OwnRedirect.ToReturn(this);
    }

    public void Afficher()
    {
        if (Users != null)
            Afficher(Users);
    }

    public void Afficher(int start)
    {
        if (Users != null)
            Afficher(Users, start);
    }

    public void Afficher(ArrayList<OwnUser> users)
    {
        Afficher(users, OwnUser.ExtractUser(users, new OwnDB().getCurrentUser().getUid()).ExtractRanking(users));
    }

    public void Afficher(ArrayList<OwnUser> users, int start) {
        ArrayList<TextView> Rank = new ArrayList<>();
        ArrayList<TextView> UserName = new ArrayList<>();
        ArrayList<TextView> Score = new ArrayList<>();
        Rank.add(findViewById(R.id.lbl_rang1_classement));
        UserName.add(findViewById(R.id.lbl_pseudo1_classement));
        Score.add(findViewById(R.id.lbl_score1_classement));

        Rank.add(findViewById(R.id.lbl_rang2_classement));
        UserName.add(findViewById(R.id.lbl_pseudo2_classement));
        Score.add(findViewById(R.id.lbl_score2_classement));

        Rank.add(findViewById(R.id.lbl_rang3_classement));
        UserName.add(findViewById(R.id.lbl_pseudo3_classement));
        Score.add(findViewById(R.id.lbl_score3_classement));

        Rank.add(findViewById(R.id.lbl_rang4_classement));
        UserName.add(findViewById(R.id.lbl_pseudo4_classement));
        Score.add(findViewById(R.id.lbl_score4_classement));

        Rank.add(findViewById(R.id.lbl_rang5_classement));
        UserName.add(findViewById(R.id.lbl_pseudo5_classement));
        Score.add(findViewById(R.id.lbl_score5_classement));

        Rank.add(findViewById(R.id.lbl_rang6_classement));
        UserName.add(findViewById(R.id.lbl_pseudo6_classement));
        Score.add(findViewById(R.id.lbl_score6_classement));

        Rank.add(findViewById(R.id.lbl_rang7_classement));
        UserName.add(findViewById(R.id.lbl_pseudo7_classement));
        Score.add(findViewById(R.id.lbl_score7_classement));

        ArrayList<OwnUser> UsersEmployed = OwnUser.ExtractSample(users, 7, start);
        if (!OwnUser.SampleIsEmpty(UsersEmployed))
        {
            int firstStart = start - 3;
            if (firstStart < 0)
                firstStart = 0;
            for (int i = 0; i < 7; i++) {
                String score = "";
                String rank = "";
                UserName.get(i).setText(UsersEmployed.get(i).username);
                if (UsersEmployed.get(i).score != OwnUser.INVALIDE_DATA)
                    score = String.valueOf(UsersEmployed.get(i).score);
                Score.get(i).setText(score);
                if (users.size() > firstStart + i)
                    rank = String.valueOf(firstStart + 1 + i);
                Rank.get(i).setText(rank);
            }
            Classement = firstStart + 3;
        }
    }

    public void UpClassement(View view) {
        Afficher(Classement - 7);
    }

    public void DownClassement(View view) {
        Afficher(Classement + 7);
    }

    public void MonClassement(View view) {
        Afficher();
    }
}
