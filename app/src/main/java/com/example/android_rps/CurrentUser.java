package com.example.android_rps;

import java.util.ArrayList;

class CurrentUser
{
    private OwnUser data;
    private final ArrayList<CurrentUserListener> currentUserListeners;

    CurrentUser()
    {
        data = null;
        currentUserListeners = new ArrayList<>();
    }

    void setData(OwnUser data)
    {
        OwnUser old = this.data;
        this.data = data;
        for (CurrentUserListener ping : currentUserListeners)
        {
            ping.CurentUserValueChanged(old, data);
        }
    }

    void addAllUsersListener(CurrentUserListener currentUserListener)
    {
        currentUserListeners.add(currentUserListener);
    }

    OwnUser getData()
    {
        return data;
    }
}
