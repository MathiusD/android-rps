package com.example.android_rps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Executor;

public class OwnDB implements Executor
{
    private DatabaseReference oDatabase;
    private FirebaseAuth oAuth;
    private final static String TAG = "OwnDB";
    private final static String USER = "users";
    private final static String LOG_ASSOCIATION = "logAssociation";
    private final static String LAST_GAME = "lastGame";
    private final static String ONLINE_GAME = "onlineGame";
    private static boolean PersistenceCall = false;
    private AllUsers allUsers;
    private CurrentUser currentUser;
    private LastMove lastMove;

    OwnDB()
    {
        if (PersistenceCall == false) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            FirebaseDatabase.getInstance().getReference().keepSynced(true);
            PersistenceCall = true;
        }
        oAuth = FirebaseAuth.getInstance();
        oDatabase = FirebaseDatabase.getInstance().getReference();
        allUsers = new AllUsers();
        currentUser = new CurrentUser();
        lastMove = new LastMove();
    }

    void SignUp(AppCompatActivity view, OwnUser user, String pass)
    {
        SignUp(view, user, pass, (Intent) null);
    }

    void SignUp(AppCompatActivity view, OwnUser user, String pass, ProgressBar progressBar)
    {
        SignUp(view, user, pass, null, progressBar);
    }

    void SignUp(AppCompatActivity view, OwnUser user, String pass, ArrayList<EditText> champs)
    {
        SignUp(view, user, pass, null, champs);
    }

    void SignUp(AppCompatActivity view, OwnUser user, String pass, Intent intent)
    {
        SignUp(view, user, pass, intent, (ProgressBar) null);
    }

    void SignUp(AppCompatActivity view, OwnUser user, String pass, Intent intent, ProgressBar progressBar)
    {
        SignUp(view, user, pass, intent, progressBar, null);
    }

    void SignUp(AppCompatActivity view, OwnUser user, String pass, Intent intent, ArrayList<EditText> champs)
    {
        SignUp(view, user, pass, intent, null, champs);
    }

    void SignUp(final AppCompatActivity view, final OwnUser user, final String pass, final Intent intent, final ProgressBar progressBar, final ArrayList<EditText> champs)
    {
        ShowIsProcessing(progressBar, champs);
        oDatabase.child(LOG_ASSOCIATION).child(user.login)
            .addListenerForSingleValueEvent(new ValueEventListener()
                {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        Log.d(TAG, "LoadMail:onRead");
                        SignUpAuth(view, user, pass, dataSnapshot.getValue(String.class), intent, progressBar, champs);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError)
                    {
                        ShowEndOfProcessing(progressBar, champs);
                        Log.w(TAG, "LoadMail:onCancelled", databaseError.toException());
                    }
                }
            );
    }

    private void SignUpAuth(final AppCompatActivity view, final OwnUser user, final String pass, String PotentiallyEmail, final Intent intent, final ProgressBar progressBar, final ArrayList<EditText> champs)
    {
        if (PotentiallyEmail == null)
        {
            oAuth.createUserWithEmailAndPassword(user.email, pass)
                .addOnCompleteListener(this,
                    task -> {
                        if (task.isSuccessful())
                        {
                            Log.d(TAG, "SignUpAuth:success");
                            SignUpDB(view, Objects.requireNonNull(oAuth.getCurrentUser()).getUid(), user, intent, progressBar, champs);
                        } else
                        {
                            ShowEndOfProcessing(progressBar, champs);
                            Log.w(TAG, "SignUpAuth:failure", task.getException());
                            ShowToast(view, Objects.requireNonNull(task.getException()).getMessage());
                        }
                    }
                );
        }
        else
        {
            ShowEndOfProcessing(progressBar, champs);
            Log.d(TAG, "SignUp:LoginIsAlreadyTaken");
            ShowToast(view, "Login Is Already Taken");
        }
    }

    private void SignUpDB(AppCompatActivity view, String uid, OwnUser user, Intent intent, ProgressBar progressBar, ArrayList<EditText> champs)
    {
        user.uid = uid;
        UpdateDB(user);
        ShowEndOfProcessing(progressBar, champs);
        ShowToast(view ,"SignUp success.");
        LaunchActivity(view, intent);
    }

    void SignIn(AppCompatActivity view, String login, String pass)
    {
        SignIn(view, login, pass, (Intent) null);
    }

    void SignIn(AppCompatActivity view, String login, String pass, ProgressBar progressBar)
    {
        SignIn(view, login, pass, null, progressBar);
    }

    void SignIn(AppCompatActivity view, String login, String pass, Intent intent)
    {
        SignIn(view, login, pass, intent, null);
    }

    void SignIn(AppCompatActivity view, String login, String pass, ArrayList<EditText> champs)
    {
        SignIn(view, login, pass, null, null, champs);
    }

    void SignIn(AppCompatActivity view, String login, String pass, Intent intent , ProgressBar progressBar)
    {
        SignIn(view, login, pass, intent, progressBar, null);
    }

    void SignIn(AppCompatActivity view, String login, String pass, ProgressBar progressBar, ArrayList<EditText> champs)
    {
        SignIn(view, login, pass, null, progressBar, champs);
    }

    void SignIn(final AppCompatActivity view, String login, final String pass, final Intent intent, final ProgressBar progressBar, final ArrayList<EditText> champs)
    {
        ShowIsProcessing(progressBar, champs);
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(login).matches())
        {
            Log.d(TAG, "ExtractEmail:IsEmail");
            SignInAuth(view, login, pass, intent, progressBar, champs);
        }
        else
        {
            Log.d(TAG, "ExtractEmail:IsLogin");
            oDatabase.child(LOG_ASSOCIATION).child(login)
                .addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d(TAG, "LoadMail:onRead");
                        SignInAuth(view, dataSnapshot.getValue(String.class), pass, intent, progressBar, champs);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError)
                    {
                        ShowEndOfProcessing(progressBar, champs);
                        Log.w(TAG, "LoadMail:onCancelled", databaseError.toException());
                    }
                }
            );
        }
    }

    private void SignInAuth(final AppCompatActivity view, String email, String pass, final Intent intent, final ProgressBar progressBar, final ArrayList<EditText> champs)
    {
        if (email != null) {
            oAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this,
                    task -> {
                        if (task.isSuccessful())
                        {
                            Log.d(TAG, "SignInAuth:success");
                            ShowEndOfProcessing(progressBar, champs);
                            LaunchActivity(view, intent);
                            ShowToast(view, "SignIn success.");
                        }
                        else
                        {
                            ShowEndOfProcessing(progressBar, champs);
                            Log.w(TAG, "SignInAuth:failure", task.getException());
                            ShowToast(view, Objects.requireNonNull(task.getException()).getMessage());
                        }
                    }
                );
        }
        else
        {
            ShowEndOfProcessing(progressBar, champs);
            Log.d(TAG, "SignIn:InvalidLoginOrMail");
            ShowToast(view, "Invalid Login Or Mail");
        }
    }

    void AddScore(AppCompatActivity view, final int nb_point)
    {
        if (UserIsLog(view))
        {
            oDatabase.child(USER).child(getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Log.d(TAG, "LoadUser:onRead");
                            OwnUser user = dataSnapshot.getValue(OwnUser.class);
                            Objects.requireNonNull(user).score += nb_point;
                            Log.d(TAG, "AddScore:Add");
                            user.uid = getCurrentUser().getUid();
                            UpdateDB(user);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.w(TAG, "LoadUser:onCancelled", databaseError.toException());
                        }
                    }
                );
        }
    }

    void ExtractUsers(boolean recurrent)
    {
        ExtractUsers(null, recurrent);
    }

    void ExtractUsers(AppCompatActivity view)
    {
        ExtractUsers(view, false);
    }

    void ExtractUsers(AppCompatActivity view, boolean recurrent)
    {
        if (UserIsLog(view))
        {
            if (recurrent == false)
                oDatabase.child(USER)
                    .addListenerForSingleValueEvent(
                        InstructionToExtractAllUsers()
                    );
            else
                oDatabase.child(USER)
                    .addValueEventListener(
                        InstructionToExtractAllUsers()
                    );
        }
    }

    private ValueEventListener InstructionToExtractAllUsers()
    {
        return new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "LoadUsers:onRead");
                ArrayList<OwnUser> TempUsers = new ArrayList<>();
                Iterable<DataSnapshot> dt = dataSnapshot.getChildren();
                for (DataSnapshot data : dt) {
                    OwnGame game;
                    game = data.child(LAST_GAME).getValue(OwnGame.class);
                    OwnUser value = data.getValue(OwnUser.class);
                    Objects.requireNonNull(value).lastGame = game;
                    TempUsers.add(value);
                }
                allUsers.setData(OwnUser.SortUsers(TempUsers));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "LoadUsers:onCancelled", databaseError.toException());
            }
        };
    }

    void ExtractCurrentUser(boolean recurrent)
    {
        ExtractCurrentUser(null, false);
    }

    void ExtractCurrentUser(AppCompatActivity view)
    {
        ExtractCurrentUser(view, false);
    }

    void ExtractCurrentUser(AppCompatActivity view, boolean recurrent)
    {
        if (UserIsLog(view))
        {
            if (recurrent == false)
                oDatabase.child(USER).child(this.getCurrentUser().getUid())
                    .addListenerForSingleValueEvent(
                        InstructionToExtractCurentUser()
                    );
            else
                oDatabase.child(USER).child(this.getCurrentUser().getUid())
                    .addValueEventListener(
                        InstructionToExtractCurentUser()
                    );
        }
    }

    private ValueEventListener InstructionToExtractCurentUser()
    {
        return new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "LoadUser:onRead");
                OwnGame game = dataSnapshot.child(LAST_GAME).getValue(OwnGame.class);
                OwnUser value = dataSnapshot.getValue(OwnUser.class);
                Objects.requireNonNull(value).lastGame = game;
                currentUser.setData(value);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "LoadUser:onCancelled", databaseError.toException());
            }
        };
    }

    private void UpdateDB(final OwnUser user)
    {
        OwnGame game = user.lastGame;
        Log.d(TAG, "UpdateDB:AppendData:" + user.toString());
        oDatabase.child(USER).child(user.uid).setValue(user);
        if (game.position > 0)
            oDatabase.child(USER).child(user.uid).child(LAST_GAME).setValue(game);
        oDatabase.child(LOG_ASSOCIATION).child(user.login).setValue(user.email);

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
            .setDisplayName(user.login)
            .build();

        getCurrentUser().updateProfile(profileUpdates)
            .addOnCompleteListener(
                task -> {
                    if (task.isSuccessful())
                    {
                        Log.d(TAG, "UpdateDB:UserProfileUpdated.");
                    }
                }
            );
    }

    private void LaunchActivity(AppCompatActivity view, Intent intent)
    {
        if (intent != null)
        {
            view.finish();
            view.startActivity(intent);
        }
    }

    void UpdateUser(AppCompatActivity view, OwnUser user)
    {
        UpdateUser(view, user, (Intent) null);
    }

    void UpdateUser(AppCompatActivity view, OwnUser user, ProgressBar progressBar)
    {
        UpdateUser(view, user, null, progressBar, null);
    }

    void UpdateUser(AppCompatActivity view, OwnUser user, Intent intent)
    {
        UpdateUser(view, user, intent, null);
    }

    void UpdateUser(AppCompatActivity view, OwnUser user, ArrayList<EditText> champs)
    {
        UpdateUser(view, user, null, null, champs);
    }

    void UpdateUser(AppCompatActivity view, OwnUser user, Intent intent, ProgressBar progressBar)
    {
        UpdateUser(view, user, intent, progressBar, null);
    }

    void UpdateUser(AppCompatActivity view, OwnUser user, ProgressBar progressBar, ArrayList<EditText> champs)
    {
        UpdateUser(view, user, null, progressBar, champs);
    }

    boolean UpdateUser(AppCompatActivity view, OwnUser user, Intent intent, ProgressBar progressBar, ArrayList<EditText> champs)
    {
        if (!UserIsLog(view))
            return false;
        if (user.uid.contentEquals(""))
        {
            user.uid = getCurrentUser().getUid();
        }
        if (getCurrentUser().getUid().contentEquals(user.uid) && (Objects.requireNonNull(getCurrentUser().getEmail()).contentEquals(user.email)) && (Objects.requireNonNull(getCurrentUser().getDisplayName()).contentEquals(user.login)))
        {
            ShowIsProcessing(progressBar, champs);
            Log.d(TAG, "UpdateUser:UserValid");
            UpdateDB(user);
            ShowEndOfProcessing(progressBar, champs);
            LaunchActivity(view, intent);
            return true;
        }
        else
        {
            Log.d(TAG, "UpdateUser:UserInvalid");
            return false;
        }
    }

    boolean UpdateOnlineGame(AppCompatActivity view, String move)
    {
        if (!UserIsLog(view))
            return false;
        else
            oDatabase.child(ONLINE_GAME).child(getCurrentUser().getUid()).setValue(move);
            return true;
    }

    void ExtractOpponentLastMove(AppCompatActivity view)
    {
        if (UserIsLog(view))
            oDatabase.child(ONLINE_GAME).child(getCurrentUser().getUid())
                .addListenerForSingleValueEvent(
                    InstructionToExtractOpponentLastMove()
                );
    }

    private ValueEventListener InstructionToExtractOpponentLastMove()
    {
        return new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "LoadMove:onRead");
                String value = dataSnapshot.getValue(String.class);
                lastMove.setData(value);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "LoadMove:onCancelled", databaseError.toException());
            }
        };
    }

    private boolean UserIsLog(AppCompatActivity view)
    {
        if (getCurrentUser() == null)
        {
            Log.d(TAG, "UserIsLog:UserNotSignIn");
            if (view != null)
                ShowToast(view, "User Not Sign In. Please Sign In.");
            return false;
        }
        else
            return true;
    }

    private void ShowIsProcessing(ProgressBar progressBar, ArrayList<EditText> champs)
    {
        if (progressBar != null)
        {
            progressBar.setVisibility(View.VISIBLE);
        }
        if (champs != null)
        {
            for (EditText editText : champs)
            {
                editText.setEnabled(false);
            }
        }
    }

    private void ShowEndOfProcessing(ProgressBar progressBar, ArrayList<EditText> champs)
    {
        if (progressBar != null)
        {
            progressBar.setVisibility(View.GONE);
        }
        if (champs != null)
        {
            for (EditText editText : champs)
            {
                editText.setEnabled(true);
            }
        }
    }

    private void ShowToast(Context ctx, String msg)
    {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    FirebaseUser getCurrentUser()
    {
        return oAuth.getCurrentUser();
    }

    AllUsers getAllUsers()
    {
        return allUsers;
    }

    CurrentUser getCurrentUserDB()
    {
        return currentUser;
    }

    LastMove getLastMove()
    {
        return lastMove;
    }

    void Disconnect()
    {
        oAuth.signOut();
    }

    @Override
    public void execute(Runnable runnable)
    {
        Log.v(TAG, "execute:" + runnable.toString());
        runnable.run();
    }
}
