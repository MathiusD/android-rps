package com.example.android_rps;

public class Difficulte {
    private String nom;
    private int chanceVictoireIA;
    private int chanceDefaiteIA;
    private int chanceEgalite;
    private int pointsGagne;


    final static Difficulte SPOCK = new Difficulte("Spock", 0, 10, 0, 0);
    final static Difficulte FACILE = new Difficulte("Facile", 2, 6, 2, 10);
    final static Difficulte NORMAL = new Difficulte("Normal", 4, 4, 2, 25);
    final static Difficulte DIFFICILE = new Difficulte("Difficile", 6, 2, 2, 50);
    final static Difficulte IMPOSSIBLE = new Difficulte("Impossible", 7, 2, 1, 75);

    private Difficulte(String nom, int chanceVictoireIA, int chanceDefaiteIA, int chanceEgalite, int pointsGagne) {
        this.nom = nom;
        this.chanceVictoireIA = chanceVictoireIA;
        this.chanceDefaiteIA = chanceDefaiteIA;
        this.chanceEgalite = chanceEgalite;
        this.pointsGagne = pointsGagne;
    }

    String getNom() {
        return nom;
    }

    int getChanceVictoireIA() {
        return chanceVictoireIA;
    }

    int getChanceDefaiteIA() {
        return chanceDefaiteIA;
    }

    int getChanceEgalite() {
        return chanceEgalite;
    }

    int getPointsGagne() {
        return pointsGagne;
    }
}
