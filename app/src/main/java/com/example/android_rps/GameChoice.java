package com.example.android_rps;

import java.util.ArrayList;

public class GameChoice {

    public final static ArrayList<GameChoice> ALL_ELEMENT = new ArrayList<>();
    public final static GameChoice SCISSORS;
    public final static GameChoice ROCK;
    public final static GameChoice PAPER;
    public final static GameChoice SPOCK;
    public final static GameChoice LIZARD;

    static
    {
        SCISSORS = new GameChoice("Ciseaux", new ArrayList<>());
        ROCK = new GameChoice("Pierre", new ArrayList<>());
        PAPER = new GameChoice("Papier", new ArrayList<>());
        SPOCK = new GameChoice("Spock", new ArrayList<>());
        LIZARD = new GameChoice("Lésard", new ArrayList<>());
        SCISSORS.advantage.add(PAPER);
        SCISSORS.advantage.add(LIZARD);
        ROCK.advantage.add(SCISSORS);
        ROCK.advantage.add(LIZARD);
        PAPER.advantage.add(ROCK);
        PAPER.advantage.add(SPOCK);
        SPOCK.advantage.add(SCISSORS);
        SPOCK.advantage.add(ROCK);
        LIZARD.advantage.add(SPOCK);
        LIZARD.advantage.add(PAPER);
        ALL_ELEMENT.add(SCISSORS);
        ALL_ELEMENT.add(ROCK);
        ALL_ELEMENT.add(PAPER);
        ALL_ELEMENT.add(SPOCK);
        ALL_ELEMENT.add(LIZARD);
    }

    private ArrayList<GameChoice> advantage;
    private String name;

    private GameChoice(String name, ArrayList<GameChoice> advantage)
    {
        this.name = name;
        this.advantage = advantage;
    }

    public byte Fight(GameChoice other)
    {
        if (this.advantage.contains(other))
            if (other.advantage.contains(this))
                return 0;
            else
                return 1;
        else
            if (other.advantage.contains(this))
                return -1;
            else
                return 0;
    }

    public ArrayList<GameChoice> Frailty()
    {
        ArrayList<GameChoice> frailty = new ArrayList<>();
        for (GameChoice other : ALL_ELEMENT)
            if (Fight(other) == -1)
                frailty.add(other);
        return frailty;
    }

    public ArrayList<GameChoice> Neutral()
    {
        ArrayList<GameChoice> neutral = new ArrayList<>();
        for (GameChoice other : ALL_ELEMENT)
            if (Fight(other) == 0)
                neutral.add(other);
        return neutral;
    }

    public String getName()
    {
        return name;
    }

    public ArrayList<GameChoice> getAdvantage()
    {
        return advantage;
    }

    @Override
    public String toString()
    {
        String str =  "GameChoice :{\"name\":\"" + name + "\",\"advantage\":[";
        for (int i = 0; i < advantage.size(); i++)
        {
            if (i > 0)
                str = str + ",";
            str = str + "\"" + advantage.get(i).getName() + "\"";
        }
        str += "],\"frailty\":[";
        ArrayList<GameChoice> frailty = Frailty();
        for (int i = 0; i < frailty.size(); i++)
        {
            if (i > 0)
                str = str + ",";
            str = str + "\"" + frailty.get(i).getName() + "\"";
        }
        str += "],\"neutral\":[";
        ArrayList<GameChoice> neutral = Neutral();
        for (int i = 0; i < neutral.size(); i++)
        {
            if (i > 0)
                str = str + ",";
            str = str + "\"" + neutral.get(i).getName() + "\"";
        }
        str += "]";
        return str;
    }
}
