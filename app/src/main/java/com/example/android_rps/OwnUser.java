package com.example.android_rps;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

public class OwnUser implements Comparable, Parcelable
{

    public String username;
    public String email;
    public String login;
    public String name;
    public String firstName;
    public String uid;
    public int score;
    public OwnGame lastGame;
    private static final String TAG = "OwnUser";
    public static final int INVALIDE_DATA = -1;

    OwnUser(String username, String email, String login, String name, String firstName, String uid, int score, OwnGame lastGame)
    {
        this.username = username;
        this.email = email;
        this.login = login;
        this.name = name;
        this.firstName = firstName;
        this.uid = uid;
        this.score = score;
        this.lastGame = lastGame;
    }

    OwnUser(String username, String email, String login, String name, String firstName)
    {
        this(username, email, login, name, firstName, "", 0, new OwnGame());
    }

    OwnUser(String username, String email, String login, String name)
    {
        this(username, email, login, name, "");
    }

    OwnUser(String username, String email, String login)
    {
        this(username, email, login, "");
    }

    OwnUser()
    {
        this("", "", "");
    }

    protected OwnUser(Parcel in)
    {
        username = in.readString();
        email = in.readString();
        login = in.readString();
        name = in.readString();
        firstName = in.readString();
        uid = in.readString();
        score = in.readInt();
        lastGame = new OwnGame();
        lastGame.difficulty = in.readString();
        lastGame.position = in.readInt();
        lastGame.manches = new ArrayList<Integer>(OwnGame.MANCHES);
        int max = in.readInt();
        for (int i = 0; i < max; i++)
        {
            lastGame.manches.add(in.readInt());
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(login);
        dest.writeString(name);
        dest.writeString(firstName);
        dest.writeString(uid);
        dest.writeInt(score);
        dest.writeString(lastGame.difficulty);
        dest.writeInt(lastGame.position);
        dest.writeInt(lastGame.manches.size());
        for (int i = 0; i < lastGame.manches.size(); i++)
        {
            dest.writeInt(lastGame.manches.get(i));
        }
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<OwnUser> CREATOR = new Creator<OwnUser>()
    {
        @Override
        public OwnUser createFromParcel(Parcel in)
        {
            return new OwnUser(in);
        }

        @Override
        public OwnUser[] newArray(int size)
        {
            return new OwnUser[size];
        }
    };

    @Override
    public String toString()
    {
        return "OwnUser:{\"username\":\"" + this.username + "\",\"email\":\"" + this.email + "\",\"login\":\"" + this.login + "\",\"score\":\"" + this.score + "\"" + ",\"name\":\"" + this.name + "\"" + ",\"firstName\":\"" + this.firstName + "\",\"uid\":\"" + this.uid + "\",\"lastGame:\":\"" + this.lastGame.toString() + "\"}";
    }

    @Override
    public int compareTo(Object o)
    {
        if (o.getClass() != OwnUser.class)
        {
            return 0;
        }
        else
        {
            OwnUser user = (OwnUser) o;
            return user.score - this.score;
        }
    }

    static ArrayList<OwnUser> SortUsers(ArrayList<OwnUser> db)
    {
        Log.d(TAG, "SortUsers:SortByScore");
        Collections.sort(db);
        return db;
    }

    int ExtractRanking(ArrayList<OwnUser> db)
    {
        Log.d(TAG, "ExtractRanking:Processing");
        for (int i = 0; i < db.size(); i++) {
            if (this.uid == db.get(i).uid)
                return i;
        }
        return -1;
    }

    static OwnUser ExtractUser(ArrayList<OwnUser> db, String uid)
    {
        Log.d(TAG, "ExtractUser:Processing");
        for (OwnUser usr : db)
        {
            if (usr.uid.equals(uid))
            {
                return usr;
            }
        }
        return null;
    }

    String Verification()
    {
        if (username.length() == 0)
            return "Le Pseudo est vide.";
        if (login.length() == 0)
            return "Le Login est vide.";
        if (email.length() == 0)
            return "L'Email est vide.";
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return "L'Email est incorrect.";
        return null;
    }

    static ArrayList<OwnUser> ExtractSample(ArrayList<OwnUser> db, int SampleSize, int start)
    {
        Log.d(TAG, "ExtractSample:Processing");
        int firstStart = start - 3;
        if (firstStart < 0)
            firstStart = 0;
        ArrayList<OwnUser> Sample = new ArrayList<>();
        for (int i = 0; i < SampleSize; i++)
        {
            if (db.size() > firstStart + i)
                Sample.add(db.get(firstStart + i));
            else
            {
                OwnUser user = new OwnUser();
                user.score = INVALIDE_DATA;
                Sample.add(user);
            }
        }
        return Sample;
    }

    static boolean SampleIsEmpty(ArrayList<OwnUser> Sample)
    {
        boolean empty = true;
        for (OwnUser user: Sample)
            if (user.score != INVALIDE_DATA)
                empty = false;
        return empty;
    }
}
