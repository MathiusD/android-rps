package com.example.android_rps;

import java.util.ArrayList;

public interface AllUsersListener
{
    void AllUsersValueChanged(ArrayList<OwnUser> OldUsers, ArrayList<OwnUser> NewUsers);
}
