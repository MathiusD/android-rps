package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class LaunchRPS extends AppCompatActivity
{

    private String TAG = "OwnTest";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_r_p_s);
    }

    public void onTest(View view)
    {
        Log.d(TAG, "LaunchTest");
        OwnDB ownDB = new OwnDB();
        OwnUser user = new OwnUser("Blap", "blap@example.org", "DarkBlap42", "Jean", "EsMar");
        OwnGame lg = new OwnGame();
        lg.EndTurn(OwnGame.WIN);
        lg.EndTurn(OwnGame.LOOSE);
        lg.EndTurn(OwnGame.EQUALITY);
        user.lastGame = lg;
        Intent intent = new Intent(this, this.getClass());
        ownDB.SignUp(this, user, "123456", intent);
        Log.d(TAG, "SignUpBlap");
        ownDB.SignIn(this, user.login, "123456");
        Log.d(TAG, "SignInBlapLoginWithoutNewActivity");
        ownDB.SignIn(this, user.email, "123456", intent);
        Log.d(TAG, "SignInBlapMail");
        ownDB.SignIn(this, user.login + "dzdfd", "123456", intent);
        Log.d(TAG, "SignInBlapWrongLogin");
        ownDB.SignIn(this, user.email, "1456", intent);
        Log.d(TAG, "SignInBlapMailWrongPassword");
        OwnUser userinva = new OwnUser("Blap", "blap2@example.org", "DarkBlap42", "Jean", "EsMar");
        ownDB.SignUp(this, userinva, "123456", intent);
        Log.d(TAG, "SignUpBlapWrongLogin");
        OwnUser useri = new OwnUser("Blap", "blap@example.org", "DarkBlap423", "Jean", "EsMar");
        ownDB.SignUp(this, useri, "123456", intent);
        Log.d(TAG, "SignUpBlapWrongMail");
        ownDB.SignIn(this, user.login, "123456", intent);
        Log.d(TAG, "SignInBlapLogin");
        ownDB.Disconnect();
        Log.d(TAG, "SignOut");
        ownDB.AddScore(this, 9);
        Log.d(TAG, "AddScoreNothing");
        ownDB.SignIn(this, user.login, "123456", intent);
        Log.d(TAG, "SignInBlapLogin");
        ownDB.AddScore(this, 18);
        Log.d(TAG, "AddScoreBlap");
        OwnUser userj = new OwnUser("Blap", "blap45@example.org", "DarkBlap423", "Jean", "EsMar");
        ownDB.SignUp(this, userj, "1245688", intent);
        Log.d(TAG, "SignUpBlap2");
        ownDB.SignIn(this, user.login, "123456", intent);
        Log.d(TAG, "SignInBlapLogin");
        ownDB.getAllUsers().addAllUsersListener(
            (OldUsers, NewUsers) -> Log.d(TAG, "NewValueIs:" + NewUsers.toString())
        );
        ownDB.ExtractUsers(this);
        Log.d(TAG, "ExtractUsers");
        ownDB.getCurrentUserDB().addAllUsersListener(
                (OldUser, NewUser) -> Log.d(TAG, "NewValueIs:" + NewUser.toString())
        );
        ownDB.ExtractCurrentUser(this);
        Log.d(TAG, "ExtractCurentUser");
        ownDB.UpdateUser(this, user);
        Log.d(TAG, "UpdateUser");
    }
}
