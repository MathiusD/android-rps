package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        if (new OwnDB().getCurrentUser() != null)
        {
            Intent destination = new Intent(this, choixDifficulteActivity.class);
            finish();
            startActivity(destination);
        }
        ProgressBar progressBar = findViewById(R.id.bar_register);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void ToLogin(View view) {
        Intent destination = new Intent(this, LoginActivity.class);
        startActivity(destination);
    }

    public void Register(View view) {
        ProgressBar progressBar = findViewById(R.id.bar_register);
        EditText prenom = findViewById(R.id.lbl_prenom_register);
        EditText nom = findViewById(R.id.lbl_nom_register);
        EditText pseudo = findViewById(R.id.lbl_pseudo_register);
        EditText email = findViewById(R.id.lbl_email_register);
        EditText password = findViewById(R.id.lbl_password_register);
        EditText username = findViewById(R.id.lbl_username_register);
        OwnDB register =  new OwnDB();
        OwnUser user = new OwnUser(
            pseudo.getText().toString(),
            email.getText().toString(),
            username.getText().toString(),
            nom.getText().toString(),
            prenom.getText().toString()
        );
        if (user.Verification() == null)
        {
            if (password.getText().toString().length() != 0)
            {
                Intent destination = new Intent(this, ReglesActivity.class);
                destination.putExtra(OwnRedirect.TO, OwnRedirect.CHOIX);
                ArrayList<EditText> champs = new ArrayList<>();
                champs.add(prenom);
                champs.add(nom);
                champs.add(pseudo);
                champs.add(email);
                champs.add(password);
                champs.add(username);
                register.SignUp(this,user,password.getText().toString(),destination, progressBar);
            }
            else
            {
                Toast.makeText(this, "Le Mot de Passe est vide.", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(this, user.Verification(), Toast.LENGTH_LONG).show();
        }
    }
}
