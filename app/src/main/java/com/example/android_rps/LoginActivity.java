package com.example.android_rps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        if (new OwnDB().getCurrentUser() != null)
        {
            startService(new Intent(this, NotificationService.class));
            Toast.makeText(this, "Bienvenue " + new OwnDB().getCurrentUser().getDisplayName(), Toast.LENGTH_LONG).show();
            Intent destination = new Intent(this, choixDifficulteActivity.class);
            finish();
            startActivity(destination);
        }
        ProgressBar progressBar = findViewById(R.id.bar_login);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void ToRegister(View view) {
        Intent destination = new Intent(this, RegisterActivity.class);
        startActivity(destination);
    }

    public void Login(View view) {
        ProgressBar progressBar = findViewById(R.id.bar_login);
        EditText username = findViewById(R.id.lbl_username_login);
        EditText password = findViewById(R.id.lbl_password_login);
        OwnDB login = new OwnDB();
        Intent destination = new Intent(this, choixDifficulteActivity.class);
        ArrayList<EditText> champs = new ArrayList<>();
        champs.add(password);
        champs.add(username);
        login.SignIn(this, username.getText().toString(), password.getText().toString(), destination, progressBar);
    }
}
