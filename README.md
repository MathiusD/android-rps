# Android-RPS

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/4fd60bff329348dbb65efb9fb3964808)](https://www.codacy.com/manual/MathiusD/android-rps?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/android-rps&amp;utm_campaign=Badge_Grade)

Ceci est un Pierre-Papier-Ciseaux sous Android pour les cours

## Auteurs

* Paul Roselle
* Mathieu Féry (aka Mathius)
